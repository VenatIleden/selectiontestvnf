using UnityEngine;
using UnityEditor;

namespace Editors
{
    /// <summary>
    /// Transforms any .png or .jpg file imported into Resources/Sprites
    /// from Texture to Sprite without any manual interaction
    /// 
    /// For more info about AssetPostprocessor: https://docs.unity3d.com/ScriptReference/AssetPostprocessor.html
    /// </summary>
    public class SpriteProcessor : AssetPostprocessor
    {
        #region Attributes

        string targetFolder = "Resources/Sprites";

        #endregion

        #region Methods

        /// <summary>
        /// Callback launched automatically when an Image with Texture2D format is
        /// imported into any folder of the project
        /// </summary>
        /// <param name="texture">File imported</param>
        void OnPostprocessTexture(Texture2D texture)
        {
            // Check the path where the file was storaged
            bool isInSpriteFolder = assetPath.IndexOf(targetFolder) != -1;

            // Change file configuration if it was storaged in the folder Resources/Sprites
            if (isInSpriteFolder)
            {
                TextureImporter importer = (TextureImporter)assetImporter;
                importer.textureType = TextureImporterType.Sprite;
            }
        }

        #endregion
    }
}